# chiseids

A redistribution of the [Chise IDS] database.

[Chise IDS]: https://gitlab.chise.org/CHISE/ids

## License

See [`LICENSE`] for more information.

[`LICENSE`]: LICENSE
